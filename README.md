# Harbor + Rancher

![harbor](harbor/assets/rancherImages/harbor.png)

## Introduction:

Harbor is a Private Registry solution which helps in managing and serving container images in secure environment. In production environments we tend to use private registry to have better control of the images and its security.

More details on how to setup Harbor with SSL certificate using openssl are discussed below. 


## Harbor Setup:
### Environment:

```shell
Distribution: Ubuntu 16.04 Desktop / Ubuntu 18.04 DesktopREADME.md
fqdn: registry-srv.stalin.io
```

### Harbor Installation Prerequisites:
All the requirement details needed to setup harbor VM can be found [here](https://goharbor.io/docs/1.10/install-config/installation-prereqs/).

### Install Docker:

```shell
stalin@registry-srv:~$ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
stalin@registry-srv:~$ sudo apt install software-properties-common python
stalin@registry-srv:~$ sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
stalin@registry-srv:~$ sudo apt-get update
stalin@registry-srv:~$ sudo apt-get install -y docker-ce

```

Verify Docker service state

```shell
sudo systemctl status docker --no-pager -l
```

Example output:

```shell
● docker.service - Docker Application Container Engine
   Loaded: loaded (/lib/systemd/system/docker.service; enabled; vendor preset: enabled)
   Active: active (running) since Fri 2020-04-10 20:49:29 IST; 2min 27s ago
     Docs: https://docs.docker.com
 Main PID: 4315 (dockerd)
   CGroup: /system.slice/docker.service
           └─4315 /usr/bin/dockerd -H fd:// --containerd=/run/containerd/containerd.sock
```

### Download docker-compose binary

```shell
stalin@registry-srv:~$ sudo curl -L "https://github.com/docker/compose/releases/download/1.25.5/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
stalin@registry-srv:~$ sudo chmod +x /usr/local/bin/docker-compose
stalin@registry-srv:~$ sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
```

### Setup Certificates

Before setting up certificates, ensure you had the following attribute commented in the `openssl.cfg` file. Otherwise, the openssl command would throw errors while generating certificates. 

```shell
#RANDFILE		= $ENV::HOME/.rnd
```

Create a staging directory first to organize the certificates

```shell
stalin@registry-srv:~$ mkdir ~/harbor_certs/
stalin@registry-srv:~$ cd ~/harbor_certs/
```

Create CA

```shell
stalin@registry-srv:~$ openssl genrsa -out ca.key 4096
stalin@registry-srv:~$ openssl req -x509 -new -nodes -sha512 -days 3650 \
 -subj "/C=IN/ST=Kerala/L=Chalakudy/O=demo/OU=Personal/CN=ca.stalin.io" \
 -key ca.key \
 -out ca.crt
```

Create SSL extension file

```shell
stalin@registry-srv:~$ cat > v3.ext <<-EOF
authorityKeyIdentifier=keyid,issuer
basicConstraints=CA:FALSE
keyUsage = digitalSignature, nonRepudiation, keyEncipherment, dataEncipherment
extendedKeyUsage = serverAuth
subjectAltName = @alt_names

[alt_names]
DNS.1=stalin.io
DNS.2=registry-srv.stalin.io
DNS.3=registry-srv
EOF
```

Create a Certificate Signing Request(CSR) for Harbor’s nginx service

```shell
stalin@registry-srv:~$ openssl genrsa -out registry-srv.stalin.io.key 4096
stalin@registry-srv:~$ openssl req -sha512 -new \
    -subj "/C=IN/ST=Kerala/L=Chalakudy/O=demo/OU=Personal/CN=registry-srv.stalin.io" \
    -key registry-srv.stalin.io.key \
    -out registry-srv.stalin.io.csr
```

Generate and Sign Certificates

```shell
stalin@registry-srv:~$ openssl x509 -req -sha512 -days 3650 \
    -extfile v3.ext \
    -CA ca.crt -CAkey ca.key -CAcreateserial \
    -in registry-srv.stalin.io.csr \
    -out registry-srv.stalin.io.crt
```

After signing , we will get output like below

```shell
Signature ok
subject=/C=IN/ST=Kerala/L=Chalakudy/O=demo/OU=Personal/CN=registry-srv.stalin.io
Getting CA Private Key
```

Create certificate directory for harbor

```shell
stalin@registry-srv:~$ sudo mkdir -p /data/cert/
stalin@registry-srv:~$ sudo cp  registry-srv.stalin.io.crt registry-srv.stalin.io.key /data/cert/
```

### Download Harbor offline installer.

```shell
stalin@registry-srv:~$ sudo curl https://storage.googleapis.com/harbor-releases/release-1.7.0/harbor-offline-installer-v1.7.1.tgz -O
stalin@registry-srv:~$ tar -xvf harbor-offline-installer-v1.7.1.tgz
```

Configure Harbor

```shell
stalin@registry-srv:~$ cd harbor
stalin@registry-srv:~$ sudo sed -i 's/hostname = reg.mydomain.com/hostname = registry-srv.stalin.io/' harbor.cfg
stalin@registry-srv:~$ sudo sed -i 's/ssl_cert = \/data\/cert\/server.crt/ssl_cert = \/data\/cert\/registry-srv.stalin.io.crt/' harbor.cfg
stalin@registry-srv:~$ sudo sed -i 's/ssl_cert_key = \/data\/cert\/server.key/ssl_cert_key = \/data\/cert\/registry-srv.stalin.io.key/' harbor.cfg
stalin@registry-srv:~$ sudo sed -i 's/ui_url_protocol = http/ui_url_protocol = https/' harbor.cfg
```

Install Harbor & Start Harbor.

```shell
stalin@registry-srv:~$ sudo ./install.sh --with-notary --with-clair --with-chartmuseum
```

You should be able to see a successful reposnse as such:

```shell
[Step 0]: checking installation environment ...

Note: docker version: 20.10.7

Note: docker-compose version: 1.25.5

[Step 1]: loading Harbor images ...
Loaded image: goharbor/registry-photon:v2.6.2-v1.7.1
Loaded image: goharbor/harbor-migrator:v1.7.1
Loaded image: goharbor/harbor-adminserver:v1.7.1
Loaded image: goharbor/harbor-core:v1.7.1
Loaded image: goharbor/harbor-log:v1.7.1
Loaded image: goharbor/harbor-jobservice:v1.7.1
Loaded image: goharbor/notary-server-photon:v0.6.1-v1.7.1
Loaded image: goharbor/clair-photon:v2.0.7-v1.7.1
Loaded image: goharbor/harbor-portal:v1.7.1
Loaded image: goharbor/harbor-db:v1.7.1
Loaded image: goharbor/redis-photon:v1.7.1
Loaded image: goharbor/nginx-photon:v1.7.1
Loaded image: goharbor/harbor-registryctl:v1.7.1
Loaded image: goharbor/notary-signer-photon:v0.6.1-v1.7.1
Loaded image: goharbor/chartmuseum-photon:v0.7.1-v1.7.1


[Step 2]: preparing environment ...
Generated and saved secret to file: /data/secretkey
Generated configuration file: ./common/config/nginx/nginx.conf
Generated configuration file: ./common/config/adminserver/env
Generated configuration file: ./common/config/core/env
Generated configuration file: ./common/config/registry/config.yml
Generated configuration file: ./common/config/db/env
Generated configuration file: ./common/config/jobservice/env
Generated configuration file: ./common/config/jobservice/config.yml
Generated configuration file: ./common/config/log/logrotate.conf
Generated configuration file: ./common/config/registryctl/env
Generated configuration file: ./common/config/core/app.conf
Generated certificate, key file: ./common/config/core/private_key.pem, cert file: ./common/config/registry/root.crt
Copying sql file for notary DB
Generated certificate, key file: ./cert_tmp/notary-signer-ca.key, cert file: ./cert_tmp/notary-signer-ca.crt
Generated certificate, key file: ./cert_tmp/notary-signer.key, cert file: ./cert_tmp/notary-signer.crt
Copying certs for notary signer
Copying notary signer configuration file
Generated configuration file: ./common/config/notary/signer-config.postgres.json
Generated configuration file: ./common/config/notary/server-config.postgres.json
Copying nginx configuration file for notary
Generated configuration file: ./common/config/nginx/conf.d/notary.server.conf
Generated and saved secret to file: /data/defaultalias
Generated configuration file: ./common/config/notary/signer_env
Generated configuration file: ./common/config/clair/postgres_env
Generated configuration file: ./common/config/clair/config.yaml
Generated configuration file: ./common/config/clair/clair_env
Create config folder: ./common/config/chartserver
Generated configuration file: ./common/config/chartserver/env
The configuration files are ready, please use docker-compose to start the service.


[Step 3]: checking existing instance of Harbor ...


[Step 4]: starting Harbor ...
Creating network "harbor_harbor" with the default driver
Creating network "harbor_harbor-clair" with the default driver
Creating network "harbor_harbor-notary" with the default driver
Creating network "harbor_harbor-chartmuseum" with the default driver
Creating network "harbor_notary-sig" with the default driver
Creating harbor-log ... done
Creating registryctl        ... done
Creating redis              ... done
Creating registry           ... done
Creating harbor-db          ... done
Creating harbor-adminserver ... done
Creating harbor-core        ... done
Creating chartmuseum        ... done
Creating harbor-jobservice  ... done
Creating harbor-portal      ... done
Creating notary-signer      ... done
Creating clair              ... done
Creating nginx              ... done
Creating notary-server      ... done

✔ ----Harbor has been installed and started successfully.----

Now you should be able to visit the admin portal at https://registry-srv.stalin.io.
For more details, please visit https://github.com/goharbor/harbor .
```

Also , you can use docker-compose to verify the health of containers


```shell
stalin@registry-srv:~$ sudo docker-compose ps
      Name                     Command                  State                                      Ports
---------------------------------------------------------------------------------------------------------------------------------------
chartmuseum         ./docker-entrypoint.sh           Up (healthy)   9999/tcp
clair               ./docker-entrypoint.sh           Up (healthy)   6060/tcp, 6061/tcp
clair-adapter       /clair-adapter/clair-adapter     Up (healthy)   8080/tcp
harbor-core         /harbor/harbor_core              Up (healthy)
harbor-db           /docker-entrypoint.sh            Up (healthy)   5432/tcp
harbor-jobservice   /harbor/harbor_jobservice  ...   Up (healthy)
harbor-log          /bin/sh -c /usr/local/bin/ ...   Up (healthy)   127.0.0.1:1514->10514/tcp
harbor-portal       nginx -g daemon off;             Up (healthy)   8080/tcp
nginx               nginx -g daemon off;             Up (healthy)   0.0.0.0:4443->4443/tcp, 0.0.0.0:80->8080/tcp, 0.0.0.0:443->8443/tcp
notary-server       /bin/sh -c migrate-patch - ...   Up
notary-signer       /bin/sh -c migrate-patch - ...   Up
redis               redis-server /etc/redis.conf     Up (healthy)   6379/tcp
registry            /home/harbor/entrypoint.sh       Up (healthy)   5000/tcp
registryctl         /home/harbor/start.sh            Up (healthy)
```

You should be able to now access the Harbor UI by using the following link: [https://registry-srv.stalin.io](https://registry-srv.stalin.io). Ensure DNS/host file entries are made. Else, use the IP address of the Harbor registry server as such: [https://10.60.99.82](https://10.60.99.82).

**NOTE:** **Punch in the IP address that you have used for your setup. The above IP is what I have used in my setup.**


You will be presented with the login page as such:

![loginPage](harbor/assets/images/loginPage.png)

### Login and Configuration:

The default username and password is:

```shell
username: admin
password: Harbor12345
```

We shall now create a new user and then we will add that user to the default public project on Harbor.

Create User:

![createUsers](harbor/assets/images/createUsers.png)

Fill the user details and set password for the account:

![newUser](harbor/assets/images/newUser.png)

Once you click on `OK` you should be able to list the user.

![users](harbor/assets/images/users.png)


Add user to the library project.

![project](harbor/assets/images/projects.png)

From there, click on `Members` and then selects `+Users`.

![libraryUsers](harbor/assets/images/libraryUser.png)

Give the user Developer Role and click on `OK`. You should be able to see the user added to the list:

![library](harbor/assets/images/library.png)

Going forward we can use this account to push images to private registry.

### Client Certificates

With the already generated CA certificate, we will generate docker client certificates to use with Rancher Kubernetes cluster. So login back to your Harbor server again and move to the directory where the CA certificates are present. In our case:

```shell
stalin@registry-srv:~$ cd ~/harbor_certs/
```

Generate a CSR for docker and get it signed for the client

```shell
stalin@registry-srv:~$ openssl genrsa -out docker-client.stalin.io.key 4096
stalin@registry-srv:~$ openssl req -sha512 -new \
    -subj "/C=IN/ST=Kerala/L=Chalakudy/O=demo/OU=Personal/CN=docker-client.stalin.io" \
    -key docker-client.stalin.io.key \
    -out docker-client.stalin.io.csr
```

Sign Certificates

```shell
stalin@registry-srv:~$ openssl x509 -req -sha512 -days 3650 \
    -extfile v3.ext \
    -CA ca.crt -CAkey ca.key -CAcreateserial \
    -in docker-client.stalin.io.csr \
    -out docker-client.stalin.io.crt
```

You will get an output like below:

```shell
Signature ok
subject=/C=IN/ST=Kerala/L=Chalakudy/O=demo/OU=Personal/CN=docker-client.stalin.io
Getting CA Private Key
```

Docker needs the certificate in PEM format , so lets convert the client certificate.

```shell
stalin@registry-srv:~$ openssl x509 -inform PEM -in docker-client.stalin.io.crt -out docker-client.stalin.io.cert
```

From here, we will make use of the harbor certificates on the k8s cluster to configure private registry. 